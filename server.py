#!/usr/bin/python

# GEC Server
# /infer?text=foo&n=1

from os import environ, path
import falcon
from falcon_cors import CORS
import json
from gec import GrammarCorrector, RerankerWeights, AugmentFeatureFlags
import logging

logging.basicConfig(level=environ.get("LOGLEVEL", "DEBUG"))

logger = logging.getLogger("gec")  # root

cors = CORS(allow_all_origins=True)

DEFAULT_MODELS_PATH = '/models/mlconv_embed'
DEFAULT_TRIE_PATH = '/models/94Bcclm.trie'
DEFAULT_DATA_PATH = '/models/data_bin'
DEFAULT_BEAM = 32


class Infer:
    def __init__(self, corrector, beam):
        self.corrector = corrector
        self.beam = beam

    def on_get(self, req, resp):
        """Handles inference GET requests"""
        logger.info("Request: {}".format(req.uri))
        text = req.get_param('text') or ''
        weights = req.get_param('weights') or ''
        nbest = min(req.get_param_as_int('n') or 1, self.beam)
        corrections = self.corrector.infer(text, nbest, weights)
        result = [
            {
                'original': text,
                'corrections': corrections,
                'weights': weights or self.corrector.weights.value
            }
        ]
        resp.body = json.dumps(result)
        resp.status = falcon.HTTP_200


# Initialize GEC
feature_str = environ.get('GEC_FEATURES', AugmentFeatureFlags.EditOps.name)
features = AugmentFeatureFlags[feature_str]
weights_str = environ.get(
    'GEC_WEIGHTS', RerankerWeights.EmbedFourEnsEditOps.name)
weights = RerankerWeights[weights_str]
models_path = environ.get('GEC_MODELS_PATH', DEFAULT_MODELS_PATH)
trie_path = environ.get('GEC_TRIE_PATH', DEFAULT_TRIE_PATH)
data_path = environ.get('GEC_DATA_PATH', DEFAULT_DATA_PATH)
beam = environ.get('GEC_BEAM', DEFAULT_BEAM)

corrector = GrammarCorrector(features, weights)
corrector.load_models(models_path, data_path, trie_path, beam)

api = falcon.API(middleware=[cors.middleware])
api.add_route('/infer', Infer(corrector, beam))

logger.info("API server is running")
