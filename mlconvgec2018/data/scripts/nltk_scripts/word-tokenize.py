#!/usr/bin/python
import sys, nltk

nltk.download('punkt')

while 1:
	line = sys.stdin.readline()
	if not line:
		break
	##line = line.decode('utf8')
	print(' '.join(nltk.word_tokenize(line.strip())).encode('utf8'))
