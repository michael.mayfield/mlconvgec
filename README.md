# mlconvgec

Derived from https://github.com/nusnlp/mlconvgec2018

The original source code has been dockerized and refactored into an API using Falcon and Gunicorn.

[GEC Presentation from 4/4/2019.](https://docs.google.com/presentation/d/14sRpM3AXcUoEMDpXCzxUfz8qOjFkTGGGVQ2exPWPL-M/edit#slide=id.p) 

## Major components of the repo
1. `mlconvgec2018` - copied directly from repo (commit 415bf08afe6eb40be4a8d7fe4b05ee37b085aebd)
2. `patches` - Modified files that are copied into `mlconvgec2018` during build time (structured this way due to dynamic dependencies). Files have been modified to support Python 3 and server-side environment.
3. `gec.py` - Implements the GEC workflow that can be invoked server-side.
4. `server.py` - Falcon-based API server
5. `Dockerfile` - A massive saga in which a research codebase is shoved into a container against its will.


# Quick Start
1. Install Docker CE (https://docs.docker.com/install/)
2. Clone this repo
3. ```cd mlconvgec```
4. Uncomment the model download lines in ```Dockerfile```
5. OR, download the latest snapshot of the EBS volume containing pretrained models.
- [Sandbox EBS volume](https://console.aws.amazon.com/ec2/v2/home?region=us-east-1#Volumes:search=vol-0f118a498dec62fec;sort=desc:createTime)
- WARNING: Either of these steps will download several hundred GBs of model data! 
6. ```docker build -t mlconvgec .```
7. ```$ V=`pwd`/models```
8. `docker run -it -v $V:/models -p8080:80 mlconvgec`
9.  Example request: 
```
$ curl "http://127.0.0.1:8080/infer?n=2&text=I%20like%20big%20dog%20."
[
  {
    "corrections": [
      {
        "features": "F0= -0.6334628462791443 EditOps0= 0 0 1",
        "hypothesis": "I like big dogs .",
        "score": "-0.6334628462791443"
      },
      {
        "features": "F0= -1.8590984344482422 EditOps0= 0 1 0",
        "hypothesis": "I like a big dog .",
        "score": "-1.8590984344482422"
      }
    ],
    "original": "I like big dog ."
  }
]
``` 

# GEC System Overview

# Limitations/Improvements

## Stabilize Server

Current GEC server is slow and often unstable. The machine doesn't seem to be busy so it is likely a self-induced bottleneck of some sort. It very well could be the file I/O dependency!!


## [done] Fix unknown words hack
Uncommon words were removed from the model as an optimization, but to replace them in the hypothesis requires access to the original source which we don't have. The current hack is to pop in words from the original sentence that are not the hypothesis, but this is often wrong.

Here is an example:
Original 
- A massive saga in which a research codebase is shoved into a container against its will.
Correction 
- A massive shoved in which a research codebase is saga into a container against its will.

## Remove noise from corrections before ranking

Example: 
```
[
  {
    "original": "She saw Tom was caught by policeman in park at last night.",
    "corrections": [
      {
        "score": "-9.365620036821337",
        "features": "F0= -9.514896392822266 EditOps0= 0 11 0   LM0= -104.8509  WordPenalty0= -23",
        "hypothesis": "She saw Tom was caught by a policeman in the park at last night. ( I think this is what you mean )"
      },
      {
        "score": "-9.755453156906496",
        "features": "F0= -9.910760879516602 EditOps0= 0 11 0   LM0= -107.6766  WordPenalty0= -23",
        "hypothesis": "She saw Tom was caught by a policeman in the park at last night. ( I think this is what you mean. )"
      },
      {
        "score": "-9.837123198482823",
        "features": "F0= -10.004927635192871 EditOps0= 0 12 0   LM0= -107.8155  WordPenalty0= -24",
        "hypothesis": "She saw Tom was caught by a policeman in the park at last night. ( I think this is what you mean ? )"
      }
    ]
  }
]
```
"I think this is what you mean ?" ... WAT?

## Accept multiple sentences
The current API assumes the text input is a single sentence. Some refactoring is required to parse sentences and handle multiple groups of corrections properly. (API format is good to go, however.)

## [done] Expose inference weights via API
We should allow the weights applied to feature score ranking to be adjusted dynamically.

## Add Spellcheck
Top results for GEC systems usually include some form of Spellcheck since the model itself doesn't deal with spellcheck well. It isn't clear where in the pipeline spelcheck is included from the paper, so some experimentation is required.

## Add LanguageTool
Add corrections from [LanguageTool](https://github.com/languagetool-org/languagetool) after the generation phase (so they are ranked with the features). This tool has a Spellcheck engine... so two birds.

## Provide insights on grammar errors
Currently the API reports corrections and feature scores, but no other metadata. Ideally we would return information about the grammatical errors that are being corrected. It turns out there is a pre-trained model to do just this: [ERRor ANnotation Toolkit: Automatically extract and classify grammatical errors in parallel original and corrected sentences](https://github.com/chrisjbryant/errant) 

This project will need to be refactored to make use of it but it should be an easier lift compared to mlconvgec2018.

## Format corrections for FE
The raw correction string isn't the most useful format for surfacing to users. The ideal UX would be to underline affected text and provide the option to apply the correction and/or learn more about the grammar error.

It turns out a common format used by the research community (M2) makes it easy to apply individual corrections to the original sentence. It also turns out that this format is returned by ERRANT. 

## Refactor for MVP
There is a lot to be desired from this codebase as is. Even if we don't have our own datasets for training there are several improvements that would make this system more maintanable:

### Refactor to leverage Pytorch 1.0  
- `mlconvgec2018` depends on the author's fork of Pytorch which is now quite out of date. Pytorch is a rapidly evolving framework so we would need to refactor this integration so Pytorch can be upgraded at a reasonable rate.
- Move our patches to internal repos. The  

# Long Term Improvement

## Inference parameter tuning
Build an ML model to optimize ranking of scores (pre-trained model score vs edit ops vs language model vs word penalty). Would require evaluation the set of corrections as a data set for training (could use 2-layer mechanical turk pipeline).

## Acquire datasets and build our own model!
There are publicly available datasets available but most (all?) disallow commercial use as most of them are provided by academic instatutions. It could be the case that commercial terms are available but some BD is required to sort this out.

Another option is that we could leverage our brand (maybe in the context of an MVP) to request data from users. The ideal form would be long-form text before and after grammar corrections. Given the many teachers and students in our audience this might quickly generate a valuable dataset.