#!/usr/bin/python
import os
import sys
from enum import Enum
from tempfile import NamedTemporaryFile, TemporaryDirectory
import importlib
import nltk
import time
import logging
logger = logging.getLogger("gec." + __name__)

# load generate
spec = importlib.util.spec_from_file_location(
    "generate", "./mlconvgec2018/software/fairseq-py/generate.py")
generate = importlib.util.module_from_spec(spec)
spec.loader.exec_module(generate)

# load nbest_reformat
spec = importlib.util.spec_from_file_location(
    "nbest_reformat", "./mlconvgec2018/scripts/nbest_reformat.py")
nbest_reformat = importlib.util.module_from_spec(spec)
spec.loader.exec_module(nbest_reformat)

# load apply_bpe
spec = importlib.util.spec_from_file_location(
    "apply_bpe", "./mlconvgec2018/scripts/apply_bpe.py")
apply_bpe = importlib.util.module_from_spec(spec)
spec.loader.exec_module(apply_bpe)

# load augmenter
from mlconvgec2018.software.reranker import augmenter
from mlconvgec2018.software.reranker import rerank
from mlconvgec2018.software.reranker.features import preload_lang_model




###############################################################################
# Port of mlconvgec2018/run.sh to Python module for suitable server-side runtime
#
# Phases
#
# Initialization
#   - collect paths to model files
#   - load models using patched version of mlconvgec2018/software/fairseq-py/generate.py
#
# Inference/generate corrections
#   - Preprocess
#       - (TODO) parse sentences
#       - Tokenize input text
#       - Apply BPE formatting
#   - Generate corrections using preloaded models (patched version of generate)
#   - Postprocess corrections
#       - Collect n best hypotheses (required??)
#       - Remove BPE formatting (required??)
#   - Ranking
#       - Reformat with nbest_reformat.py
#       - Augment by updating score with edit ops and language model (optional)
#       - Rerank using updated score
###############################################################################


# load generate
spec = importlib.util.spec_from_file_location(
    "generate", "./mlconvgec2018/software/fairseq-py/generate.py")
generate = importlib.util.module_from_spec(spec)
spec.loader.exec_module(generate)

# load nbest_reformat
spec = importlib.util.spec_from_file_location(
    "nbest_reformat", "./mlconvgec2018/scripts/nbest_reformat.py")
nbest_reformat = importlib.util.module_from_spec(spec)
spec.loader.exec_module(nbest_reformat)

# load apply_bpe
spec = importlib.util.spec_from_file_location(
    "apply_bpe", "./mlconvgec2018/scripts/apply_bpe.py")
apply_bpe = importlib.util.module_from_spec(spec)
spec.loader.exec_module(apply_bpe)



# Hack to avoid refactoring args objects throughout
class AttrDict(dict):
    def __init__(self, *args, **kwargs):
        super(AttrDict, self).__init__(*args, **kwargs)
        self.__dict__ = self

def write_lines(file_path, lines):
    fp = open(file_path, mode='w+t')
    fp.write('\n'.join(str(line).strip() for line in lines))
    fp.close()


def read_lines(file_path):
    fp = open(file_path)
    lines = fp.readlines()
    fp.close()
    return lines

class GrammarCorrectorError(Exception):
    pass


class AugmentFeatureFlags(Enum):
    EditOps = 'eo'
    EditOpsLangModel = 'eolm'


class RerankerWeights(Enum):
    # mlconv_4ens_eo.weights.txt
    # mlconv_embed_4ens_eo.weights.txt
    # mlconv_embed_4ens_eo_lm.weights.txt
    FourEnsEditOps = '0.958094 0.00365446 -0.00984251 0.0284086'
    EmbedFourEnsEditOps = '0.97223 0.00278882 -0.000467454 0.0245133'
    EmbedFourEnsEditOpsLangModel = '0.94064 -0.0208803 -0.00450021 0.015532 0.00618153 -0.0122658'


class GrammarCorrector:
    def __init__(self, features, weights):
        logger.info("GrammarCorrector {} {}".format(features, weights))
        if not isinstance(features, AugmentFeatureFlags):
            raise GrammarCorrectorError(
                "Augment feature must be one of ('{}')".format(list(AugmentFeatureFlags)))
        self.features = features or AugmentFeatureFlags.EditOps
        if not isinstance(weights, RerankerWeights):
            raise GrammarCorrectorError(
                "Weights enum must be one of ('{}')".format(list(RerankerWeights)))
        self.weights = weights or RerankerWeights.FourEnsEditOps
        self.models_loaded = False
        self.models_dir = ''
        self.lang_model_path = ''
        self.model_paths = []
        self.models = None
        self.dataset = None
        self.lang_model = None

    def load_models(self, models_dir, data_dir, lang_model_path=None, beam=12):
        if not os.path.exists(models_dir):
            raise GrammarCorrectorError(
                "Models directory ('{}') doesn't exist.".format(models_dir))
        if not os.path.exists(data_dir):
            raise GrammarCorrectorError(
                "Data directory ('{}') doesn't exist.".format(data_dir))
        # Find all pt models in model dir
        model_paths = []
        for file in os.listdir(models_dir):
            if file.endswith(".pt"):
                model_paths.append(os.path.join(models_dir, file))
        if len(model_paths) == 0:
            raise GrammarCorrectorError(
                "No valid models in directory ('{}').".format(models_dir))
        
        
        self.models_dir = models_dir
        self.model_paths = model_paths
        self.lang_model_path = lang_model_path
        
        start = time.time()

        # invoke generate.pre_load_models
        args = AttrDict({
            'path': model_paths,
            'data': data_dir,
            'gen_subset': beam,
            'beam': beam
        })
        logger.debug("Loading models with args: {}".format(args))
        models, dataset = generate.preload_models(args)
        self.models = models
        self.dataset = dataset
        
        # if LM is in use, load trie
        if self.weights == RerankerWeights.EmbedFourEnsEditOpsLangModel:
            if not os.path.exists(lang_model_path):
                raise GrammarCorrectorError(
                    "'{}' is not a valid trie Language Model.".format(lang_model_path))
            logger.debug("Loading language model: {}".format(lang_model_path))
            self.lang_model = preload_lang_model(lang_model_path)

        end = time.time()
        logger.info("Model loading complete in {} seconds.".format(end - start))

        
            
        self.models_loaded = True

    def generate(self, text, nbest):
        if not self.models_loaded:
            raise GrammarCorrectorError(
                'Models must be loaded before calling generate')
        args = AttrDict({
            'path': self.model_paths,
            'beam': nbest,
            'nbest': nbest,
            'gen_subset': nbest,
            'generate_input': text,
            'generate_output': []
        })
        logger.debug("Generating corrections with args: {}".format(args))
        # Force use of preloaded models/dataset
        generate.generate_with_models(args, self.models, self.dataset)
        corrections = args.generate_output
        logger.debug("Corrections complete.")
        return corrections

    def rank(self, processed_text, corrections, dynamic_weights):
        logger.debug("rank corrections ({}) with source text ({})".format(corrections, processed_text))
        # prepare features
        # TODO refactor flags so we can pass them to process_features
        features = augmenter.process_features(self.features.value, self.lang_model_path, self.lang_model)

        logger.debug("features: {}".format(features))

        # write tokenized source file
        source_tmp_file = NamedTemporaryFile(mode='w+t')
        write_lines(source_tmp_file.name, processed_text)

        # write nbest file for augment
        # Reformat with nbest_reformat.py
        reformatted_corrections = nbest_reformat.parse_input(
            AttrDict({'debpe': True}), corrections)
        logger.debug("reformatted_corrections: {}".format(reformatted_corrections))
        nbest_tmp_file = NamedTemporaryFile(mode='w+t')
        write_lines(nbest_tmp_file.name, reformatted_corrections)

        # Augment by updating score with approptiate weights and LM (optional)
        # Create tmp file for output
        nbest_augmented_tmp_file = NamedTemporaryFile(mode='w+t')

        logger.debug("source_tmp_file contents: {}".format(
            read_lines(source_tmp_file.name)))
        logger.debug("nbest_tmp_file contents: {}".format(
            read_lines(nbest_tmp_file.name)))

        augmenter.augment(features, source_tmp_file.name,
                                         nbest_tmp_file.name, nbest_augmented_tmp_file.name)
        
        logger.debug("nbest_augmented_tmp_file contents: {}".format(
            nbest_augmented_tmp_file.readlines()))
        
        # Rerank using updated score
        args = AttrDict({
            'out_dir': '/out',
            'input_nbest': nbest_augmented_tmp_file.name,
            'weights': dynamic_weights or self.weights.value,
            'clean_up': True
        })
        rerank.rerank(args)
        # read final output
        reranked_file_path = os.path.join(args.out_dir, \
            os.path.basename(args.input_nbest) + '.reranked.nbest')
        fp = open(reranked_file_path)
        final_corrections = fp.readlines()
        fp.close()
        return final_corrections

    def process_text(self, text):
        if not self.models_loaded:
            raise GrammarCorrectorError(
                'Models must be loaded before calling process_text')
        # TODO split into sentences
        # tokenize
        tok = ' '.join(nltk.word_tokenize(text.strip()))
        logger.debug("Tokenized text: {}".format(tok))
        # BPE
        logger.debug("Models dir: {}".format(self.models_dir))
        bpe_path = os.path.join(self.models_dir, '../bpe_model/train.bpe.model')
        fp = open(bpe_path)
        apply_bpe.apply(AttrDict({
            'codes': fp,
            'input': [tok],
            'output': NamedTemporaryFile(mode='w+t'),
            'separator': '@@'
        }))
        fp.close()
        processed_text = text
        return [processed_text]

    def infer(self, text, nbest, weights):
        logger.debug("Inference: text - '{}', nbest - {}, weights: '{}'".format(text, nbest, weights))
        start = time.time()
        processed_text = self.process_text(text)
        logger.debug("Processed text: {}".format(processed_text))
        corrections = self.generate(processed_text, nbest)
        logger.debug("Generated corrections: {}".format(corrections))
        if len(corrections) > 1:
            corrections = self.rank(processed_text, corrections, weights)
            logger.debug("Ranked corrections: {}".format(corrections))
        final_corrections = []
        for correction in corrections:
            parts = correction.split("|||")
            try:
                final_corrections.append({
                    'hypothesis': parts[1].strip(),
                    'features': parts[2].strip(),
                    'score': parts[3].strip()    
                })
            except ValueError:
                logger.warn("Ignoring hypothesis. Unexpected correction format: {}".format(correction))
        end = time.time()
        logger.info("Inference complete in {} seconds.".format(end - start))
        return final_corrections

