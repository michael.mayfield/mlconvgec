#!/usr/bin/python
import os, sys
from . import features
from mlconvgec2018.software.reranker.features import LM, EditOps, WordPenalty
from mlconvgec2018.software.reranker.candidatesreader import NBestList
import time
import numpy as np
import codecs
import argparse
import logging
from . import log_utils as L

# Initializing the logging module
logger = logging.getLogger("gec." + __name__)


def augment(features, source_path, input_nbest_path, output_nbest_path):
    ''' Function to augment the n-best list with a feature function
     :param feature: The feature function object
     :param source_path: Path to the original source sentences (maybe required for the feature function)
     :param input_nbest_path: Path to the n-best file
     :param output_nbest_path: Path to the output n-best file
    '''

    # Initialize NBestList objects
    logger.info('Initializing Nbest lists')
    input_nbest = NBestList(input_nbest_path, mode='r')
    output_nbest = NBestList(output_nbest_path, mode='w')

    # Load the source sentences
    logger.info('Loading source sentences')
    src_sents = codecs.open(source_path, mode='r', encoding='UTF-8')

    
    # For each of the item in the n-best list, append the feature
    sent_count = 0
    feature_count = 0
    for group, src_sent in zip(input_nbest, src_sents):
        logger.debug("group.size: {}, \n src_sent: {}".format(group.size(), src_sent))
        candidate_count = 0
        feature_count = 0
        for item in group:
            logger.debug("Item hyp: {}".format(item.hyp))
            for feature in features:
                item.append_feature(feature.name, feature.get_score(
                    src_sent, item.hyp, (sent_count, candidate_count)))
                feature_count += 1
            output_nbest.write(item)
            candidate_count += 1
        sent_count += 1
        #if (sent_count % 100 == 0):
        #    logger.info('Augmented ' +
        #                L.b_yellow(str(sent_count)) + ' sentences.')
    logger.info("Augmented {} sentences with {} features.".format(sent_count, feature_count))
    output_nbest.close()


def eval_features(feature_string):
    return eval('['+feature_string+']')

def process_features(feature_flag_str, lm_path, lm):
    features = []
    if feature_flag_str == 'eo':
        features.append(EditOps(name='EditOps0'))
    elif feature_flag_str == 'eolm' and lm:
        features.append(EditOps(name='EditOps0'))
        features.append(LM('LM0', lm_path, normalize=False, lang_model=lm))
        features.append(WordPenalty(name='WordPenalty0'))
    return features

def parse_args():
    parser = argparse.ArgumentParser()
    parser.add_argument("-s", "--source-sentence-file", dest="source_path",
                        required=True, help="Path to the file containing source sentences.")
    parser.add_argument("-i", "--input-nbest", dest="input_nbest_path",
                        required=True, help="Input n-best file")
    parser.add_argument("-o", "--output-nbest", dest="output_nbest_path",
                        required=True, help="Output n-best file")
    parser.add_argument("-f", "--feature", dest="feature_string", required=True,
                        help="feature initializer, e.g. LM('LM0','/path/to/lm_file', normalize=True)")
    return parser.parse_args()


if __name__ == '__main__':
    args = parse_args()
    L.set_logger(os.path.abspath(os.path.dirname(
        args.output_nbest_path)), 'augment_log.txt')
    L.print_args(args)
    features = eval_features(args.feature_string)
    augment(features, args.source_path,
            args.input_nbest_path, args.output_nbest_path)
    logger.info(L.green('Augmenting done.'))
