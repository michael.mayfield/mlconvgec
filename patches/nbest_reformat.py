#!/usr/bin/python

import argparse
import logging
logger = logging.getLogger("gec." + __name__)


def parse_args():
    parser = argparse.ArgumentParser()
    parser.add_argument('-i', '--input-file',
                        help='path to input file (output of fairseq)')
    parser.add_argument('--debpe',  action='store_true',
                        help='enable the flag to post-process and remove BPE segmentation.')

    return parser.parse_args()


def parse_input(args, lines=[]):
    logger.debug("parse_input: {}, lines: {}".format(args, lines))
    processed_lines = []
    scount = -1

    def process_line(line, scount):
        #logger.debug("process_line: {}".format(line))
        line = line.strip()
        pieces = line.split('\t')
        if pieces[0] == 'S':
            scount += 1
        if pieces[0] == 'H':
            hyp = pieces[2]
            if args.debpe:
                hyp = hyp.replace('@@ ', '')
            score = pieces[1]
            line = ("%d ||| %s ||| F0= %s ||| %s" %
                    (scount, hyp, score, score))
            processed_lines.append(line)
        return scount

    if len(lines) > 0:
        for line in lines:
            scount = process_line(line, scount)
    else:
        with open(args.input_file) as f:
            for line in f:
                scount = process_line(line, scount)

    return processed_lines


if __name__ == '__main__':
    args = parse_args()
    parse_input(args)
