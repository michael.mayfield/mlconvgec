# Derived from pytorch Dockerfile https://github.com/pytorch/pytorch/blob/a03e5cb40938b6b3f3e6dbddf9cff8afdff72d1b/Dockerfile
FROM nvidia/cuda:8.0-cudnn6-devel-ubuntu16.04

##### 
# Build environment
#####

RUN echo "deb http://developer.download.nvidia.com/compute/machine-learning/repos/ubuntu1604/x86_64 /" > /etc/apt/sources.list.d/nvidia-ml.list

RUN apt-get update && apt-get install -y --no-install-recommends \
    build-essential \
    cmake \
    git \
    curl \
    vim \
    ca-certificates \
    libjpeg-dev \
    libpng-dev &&\
    rm -rf /var/lib/apt/lists/*

RUN curl -o ~/miniconda.sh -O  https://repo.continuum.io/miniconda/Miniconda3-4.2.12-Linux-x86_64.sh  && \
    chmod +x ~/miniconda.sh && \
    ~/miniconda.sh -b -p /opt/conda && \     
    rm ~/miniconda.sh && \
    /opt/conda/bin/conda install conda-build && \
    /opt/conda/bin/conda create -y --name pytorch-py35 python=3.5.2 numpy pyyaml scipy ipython mkl&& \
    /opt/conda/bin/conda clean -ya 

##### 
# Build pytorch
#####

# Download required pytorch version
RUN git clone https://github.com/pytorch/pytorch.git && \
    cd pytorch && \
    git reset --hard a03e5cb40938b6b3f3e6dbddf9cff8afdff72d1b && \
    git submodule update --init 

ENV PATH /opt/conda/envs/pytorch-py35/bin:$PATH
RUN conda install --name pytorch-py35 -c soumith magma-cuda80 && \
    conda install --name pytorch-py35 gcc numpy cudnn nccl && \
    pip install cmake && \
    pip install cffi

WORKDIR /pytorch

RUN TORCH_CUDA_ARCH_LIST="3.5 5.2 6.0 6.1+PTX" TORCH_NVCC_FLAGS="-Xfatbin -compress-all" \
    CMAKE_PREFIX_PATH="$(dirname $(which conda))/../" \
    pip install -v .

WORKDIR /

##### 
# Download software for mlconvgec2018
#####

COPY /mlconvgec2018/software /mlconvgec2018/software

# TODO Move this to apt get install above
RUN apt-get update && apt-get install -y --no-install-recommends \
    wget \
    unzip && \
    rm -rf /var/lib/apt/lists/* && \
    cd mlconvgec2018/software && chmod u+X download.sh && ./download.sh

##### 
# Build modified fairseq-py
#####

ENV LANG=C.UTF-8

WORKDIR /mlconvgec2018/software/fairseq-py

RUN pip install -r requirements.txt && \
    python setup.py build && \
    python setup.py develop

WORKDIR /

##### 
# Uncomment these lines to download model files. 
# WARNING: This will download ~8GB pretrained grammar models (*.pt) and ~150 GB language model (*.trie).
#####

COPY mlconvgec2018/models models

#RUN cd models && chmod u+X download.sh && ./download.sh

# Instead just copy the download script
COPY download.sh .

#####
# Prepare python env
#####

# Not sure why /usr/bin/python is blown away, but fix it for now
RUN ln -s /opt/conda/envs/pytorch-py35/bin/python /usr/bin/python

RUN ["/bin/bash", "-c", "source activate pytorch-py35 && pip install nltk kenlm falcon falcon-cors gunicorn"]

# Make sure NLTK has the Punkt pickle
RUN ["/usr/bin/python", "-m", "nltk.downloader", "punkt"]

##### 
# Copy rest of required mlconv files
#####
COPY server.py server.py
COPY gec.py gec.py
COPY mlconvgec2018/data mlconvgec2018/data
COPY mlconvgec2018/scripts mlconvgec2018/scripts
COPY mlconvgec2018/run.sh mlconvgec2018/run.sh
COPY mlconvgec2018/paths.sh mlconvgec2018/paths.sh

# Copy patches
COPY patches/candidatesreader.py mlconvgec2018/software/nbest-reranker/candidatesreader.py
COPY patches/features.py mlconvgec2018/software/nbest-reranker/features.py
COPY patches/augmenter.py mlconvgec2018/software/nbest-reranker/augmenter.py
COPY patches/rerank.py mlconvgec2018/software/nbest-reranker/rerank.py
COPY patches/generate.py mlconvgec2018/software/fairseq-py/generate.py
COPY patches/nbest_reformat.py mlconvgec2018/scripts/nbest_reformat.py
COPY patches/apply_bpe.py mlconvgec2018/scripts/apply_bpe.py
COPY patches/run.sh mlconvgec2018/run.sh

# rename nbest-reranker so it can actually be imported properly
RUN ["/bin/bash", "-c", "mv mlconvgec2018/software/nbest-reranker mlconvgec2018/software/reranker"]

# Add __init__.py as necessary for proper package importing
COPY __init__.py mlconvgec2018/__init__.py
COPY __init__.py mlconvgec2018/scripts/__init__.py
COPY __init__.py mlconvgec2018/software/__init__.py
COPY __init__.py mlconvgec2018/software/reranker/__init__.py


EXPOSE 8000
WORKDIR /

# We need a very long timeout for model loading
CMD ["gunicorn", "--bind", "0.0.0.0:8000", "--timeout", "600", \
    "--access-logfile", "-", "--error-logfile", "-", "--log-level", "DEBUG",\
    "server:api"]

# See README.md for usage info.